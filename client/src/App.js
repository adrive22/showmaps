import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
import InitialShowList from "./Pages/InitialShowList/initialshowlist";


class App extends Component {
render(){
    return(

  <Router>
      <div>
          <Route path="/" component={InitialShowList} />
        </div>
    </Router>
    )
}
}
export default App;
