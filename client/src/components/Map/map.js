import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';
import React, {Component} from "react";




export class MapContainer extends Component {
  render() {
    return (
      <Map 
        google={this.props.google} 
        center={this.props.center} 
        zoom={15}>
        {this.props.children}
        
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyCAlFmQyXK6R6C19oWc9LJRfyuEzMv0zeY"
})(MapContainer)