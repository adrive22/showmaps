import React, {Component} from "react";
import API from "../../utils/API.js";
import ShowlistContainer from "../../components/showlistContainer.js";
import List from "../../components/List/List.js";
import moment from "moment";
import Map from "../../components/Map/map.js";
import { Marker, InfoWindow } from "google-maps-react";
import Banner from "../../components/Banner/banner.js";


class InitialShowList extends Component{
    state = {
        shows:[],
        locations: [],
        selectedPlace: {}
    };


componentDidMount = () => {
        this.loadShows();
        this.getLocation();
       // this.maybeRenderMap();
      
    };

    getLocation = () =>  {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                window.setTimeout(() => {
                        this.setState({
                            myLatLng: {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            }
                        }
                    );
                }, 1000)  
            })
            
        } else {
            //browser doesn't support geolocation, set as vancouver
            this.setState({
                    myLatLng: {
                        lat: 49.8527,
                        lng: -123.1207
                    }
                }
            );
        }
        
      }
      
      //create a for loop that will loop through the locations array and will create a marker for every location. 
      
      
      
      
      





loadShows = () => {
    let locations = [...this.state.locations];

    API.getShows()
    .then(res => {
        let data = res.data;
        //console.log(data)
    
        let myArr = [];

        data.forEach((item) => {
            //console.log(date)
            if(!item.placeName) return;
           
            const today=moment().format("MMM Do YYYY")
            //console.log(today)
            
            
            

            if(item.date===today){
                myArr.push({ 
                    location: [item.location],
                    //placeName: item.placeName,
                     //artist: item.artist,
                    //time: item.time,
                    // date: item.date
                })

                locations.push({
                    lat: parseFloat(item.location.split(",")[0].split(":")[1].trim()),
                    lng: parseFloat(item.location.split(",")[1].split(":")[1].trim()),
                    placeName: item.placeName,
                    artist: item.artist,
                    time: item.time,
                    date: item.date
                    
                })

               this.setState({
                   locations:locations
               })
               
                console.log(this.state.locations)
            }
            
            
            })

            this.setState({
                shows: myArr
            })
          console.log(myArr)
            // placeName: this.item.placeName,
            // artist: this.item.artist,
            // time: this.item.time,
            // date: this.item.date
            // })}
            // console.log(state.placeId)
            // })
            
            //.catch(err=>console.log(err)); 
    })
}

onMarkerClick = (props, marker, e) => {
    this.setState({
        selectedPlace: props,
        yourLocation:"Your Location",
        activeMarker: marker,
        showingInfoWindow: true,
        showingCurrentLocation: false
    });
}


onCurrentLocationClick = (props, marker) => {
    this.setState({
        showingInfoWindow: false,
        showingCurrentLocation: true,
        activeMarker: marker
    })
}



maybeRenderMap(props){
    const {google}=this.props;
    if( this.state.locations.length ){
        return (
            <Map center={this.state.myLatLng} >
            
                { 
                    this.state.locations.map( latLng => {
                        return <Marker 
                                    onClick={this.onMarkerClick}
                                    name={'Current location'}
                                    secondName="Another thing"
                                    placeName={latLng.placeName}
                                    artist={latLng.artist}
                                    time={latLng.time}
                                    date={latLng.date}
                                    position={latLng}
                                    key={ JSON.stringify(latLng) }
                                />
                            }
                        ) 
                    }

               
                <InfoWindow
                    marker={this.state.activeMarker}
                    onOpen={this.windowHasOpened}
                    onClose={this.windowHasClosed}
                    visible={this.state.showingInfoWindow}>
                    
                        <div>
                            <h1 className="marker"> { this.state.selectedPlace.placeName } </h1>
                            <p className="markertwo"> { this.state.selectedPlace.artist } </p>
                            <p className="markertwo"> { this.state.selectedPlace.time } </p>
                            
                        </div>
                    
                
           
                </InfoWindow>      
               
            
                 <Marker
                    
                    onClick={this.onCurrentLocationClick}
                    position={this.state.myLatLng}
                    icon={{
                     url:require("../../Images/mapCenter.png"),
                     }}

                />

                <InfoWindow
                     marker={this.state.activeMarker}
                     visible={this.state.showingCurrentLocation}>
                    <div>
                        <h1 className="marker"> Current Location </h1>
                    </div>
            </InfoWindow> 
                
                
        </Map>  
    
        
        )     
    }
}




render() {
    return(
        
        
        <ShowlistContainer>
            <Banner/>
            { this.maybeRenderMap()}
                
           
        </ShowlistContainer>
        
    )
}
}

export default InitialShowList;

//{this.state.shows.map( item => <div>{ JSON.stringify(item, null, 2) }</div> )}