Show Maps is an innovative way for users to see what shows are happening near them in the Austin area, without having
to cross reference each show individually into google maps. This is a huge time saver! When a user opens the website,
the app will center the map on their location, and set a blue marker with an info window that says "Your Location". It 
will also pull shows from the Mlab database that are happening for that day by cross referencing the day of the show with today.
It will then set markers for all shows in red on the map. When any of the markers for a show is clicked, the info window will show 
the venue name, the artist playing, and the time of the show. 

This app uses Mongo DB with MLab, Mongoose, Express, React, Node, Axios, Google Maps API with Geolocation, momentjs, 
and the google-maps-react npm package to help integrate the Google Maps API with React. 