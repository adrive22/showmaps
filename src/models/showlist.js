const mongoose = require("mongoose");

//saving reference to the schema constructor
const Schema = mongoose.Schema;

//using the schema constructor, create a new UserSchema object
const ShowlistSchema = new Schema({
    // `title` is required and of type String
    location: {
      type: String,
    },

    placeName: {
      type: String,
    },
    
    date: {
      type: String,
    },
    
    time: {
      type: String,
    },

    artist: {
        type: String
    }
  });
  
  // This creates our model from the above schema, using mongoose's model method
  const Showlist = mongoose.model("showlist", ShowlistSchema);
  
  
  module.exports = Showlist;