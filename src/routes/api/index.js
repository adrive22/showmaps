const router = require("express").Router();
const showlistRoutes = require("./showlistroutes.js");
const request = require('request');
const db = require('../../models');


router.use("/showlist", showlistRoutes);

module.exports = router;

