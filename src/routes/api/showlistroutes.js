const router = require("express").Router();
const showlistController = require("../../controllers/showlist_controller.js");

router.route("/")
    .get(showlistController.findAll)
    .post(showlistController.create)


module.exports = router;
    


