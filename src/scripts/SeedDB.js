const mongoose = require("mongoose");
const db = require("../models");
mongoose.Promise = global.Promise;

//heroku 
// mongoose.connect(
//     process.env.MONGODB_URI ||  "mongodb://adrive22:Guster22@ds223161.mlab.com:23161/showlistdb"
// );
// // // local
mongoose.connect(
    process.env.MONGODB_URI ||  "mongodb://localhost/showlistdb"
);



 const ShowlistSeed=[
        
    {
        "location": "lat:30.268501, lng:-97.736258",
        "placeName": "Stubbs",
        "artist": "The Lagoons, The Bishops",
        "time": "8:00",
        "date": "Jul 12th 2018"
    },{ 
        "location": "lat:30.2846,  lng:-97.7049",
        "placeName":  "The Skylark Lounge",
        "artist": "D-Soul Davis feat. Art of Soul",
        "time": "9:00",
        "date": "Jul 12th 2018"
    },{ 
        "location": "lat:30.2795, lng:-97.6816",
        "placeName":  "The Sahara Lounge",
        "artist": "Kuwa Kubwa Benefit w/ Neshama, Queen Deelah, David Sha Alheem, Oscar Ornelas, DJ Urban Mello",
        "time": "9:00",
        "date": "Jul 12th 2018"
    },{
        "location":  "lat:30.263413, lng:-97.727159",
        "placeName": "Hotel Vegas",
        "artist": "Blood Pumps, Animal Show, Teenage Cavegirl, Werewolf",
        "time": "9:00",
        "date": "Jul 12th 2018"
    },{ 
        "location": "lat:30.2665, lng:-97.7378",
        "placeName":  "Flamingo Cantina",
        "artist": "Etana, Dj Jah Bill",
        "time": "6:00",
        "date": "Jul 12th 2018"
    },{ 
        "location": "lat:30.265598,  lng:-97.743249",
        "placeName":  "The Elephant Room",
        "artist": "Daniel DuFour-tet, Wilson Marks Trio",
        "time": "6:00",
        "date": "Jul 12th 2018"
    },{ 
        "location": "lat:30.2452, lng:-97.7689",
        "placeName":  "The Broken Spoke",
        "artist": "Serailers, Rollfast Ramblers",
        "time": "6:00",
        "date": "Jul 12th 2018"
    },{
        "location": "lat:30.267708, lng:-97.736600",
        "placeName": "Beerland",
        "artist": "Proud Parent, Xetas, Kay Odyssey, Hi, Gene",
        "time": "8:00",
        "date": "Jul 12th 2018"
    },{
        "location": "lat:30.266980, lng:-97.736287",
        "placeName": "Barracuda",
        "artist": "Petal, Camp Cope, Sidney Gish",
        "time": "7:00",
        "date": "Jul 12th 2018"
    },{
        "location": "lat:30.2696,  lng:-97.7364",
        "placeName": "Cheer Up Charlies",
        "artist": "Golden Donna, Bridle, Private Service, DJ Troller, DJ Scorpio",
        "time": "8:00",
        "date": "Jul 12th 2018"
    },{
        "location": "lat:30.264750, lng:-97.7341",
        "placeName": "North Door",
        "artist": "Aunti, My Education, Excited States",
        "time": "9:00",
        "date": "Jul 12th 2018"
    },{ 
        "location": "lat:30.2682,  lng:-97.7417",
        "placeName":  "The Driskell",
        "artist": "Big John Mills",
        "time": "8:00",
        "date": "Jul 12th 2018"
    },{ 
        "location": "lat:30.2571,  lng:-97.7237",
        "placeName":  "Stay Gold",
        "artist": "The Brian Kremer Quintet",
        "time": "9:00",
        "date": "Jul 12th 2018"
    },{
        "location": "lat:30.266980, lng:-97.7362",
        "placeName": "Barracuda",
        "artist": "The Donkeys, Sweet Spirit, Mean Jolene, Buxton, Talkies",
        "time": "9:00",
        "date": "Jul 13th 2018"
    },{
        "location": "lat:30.2696, lng:-97.7364",
        "placeName": "Cheer Up Charlies",
        "artist": "Dreambeamer, Dwight Smith, Much 2 Much",
        "time": "7:00",
        "date": "Jul 13th 2018"
    },{ 
        "location": "lat:30.2674,  lng:-97.7361",
        "placeName":  "Empire Control Room",
        "artist": "Comethazine",
        "time": "9:00",
        "date": "Jul 13th 2018"
    },{ 
        "location": "lat:30.2674, lng:-97.7361",
        "placeName": "Empire Control Room",
        "artist": "Deadelus, Holly, Wylie Cable",
        "time": "9:00",
        "date": "Jul 14th 2018"
    },{ 
        "location": "lat:30.2665, lng:-97.7378",
        "placeName":  "Flamingo Cantina",
        "artist": "A Tiger Liliy, Papa Muse, Roleros Cosmicos",
        "time": "9:00",
        "date": "Jul 14th 1018"
    },{
        "location":  "lat:30.263413, lng:-97.727159",
        "placeName": "Hotel Vegas",
        "artist": "Cumbia Night w/ El Tule",
        "time": "9:00",
        "date": "Jul 14th 2018"
    },{
        "location": "lat:30.2702, lng:-97.7360",
        "placeName": "Mohawk",
        "artist": "The Bearer, The Vinous, Speaker, Drip-Fed",
        "time": "9:00",
        "date": "Jul 14th 2018"
    },{
        "location": "lat:30.266980, lng:-97.736287",
        "placeName": "Barracuda",
        "artist": "Summer Salt, Hot Flash Heat Wave, The Symposium",
        "time": "9:00",
        "date": "Jul 14th 2018"
    },{
        "location": "lat:30.268501, lng:-97.736258",
        "placeName": "Stubbs",
        "artist": "Thomas Csorba, Willy McGee",
        "time": "9:00",
        "date": "Jul 14th 2018"
    },{ 
        "location": "lat:30.2571, lng:-97.7237",
        "placeName":  "Stay Gold",
        "artist": "Terrell Shahid",
        "time": "10:00",
        "date": "Jul 14th 2018"
    },{ 
        "location": "lat:30.2452, lng:-97.7689",
        "placeName":  "The A.B.G.B.",
        "artist": "Altamesa, Marijuana Sweet Tooth",
        "time": "9:00",
        "date": "Jul 14th 2018"
    },{ 
        "location": "lat:30.2452, lng:-97.7689",
        "placeName":  "The Broken Spoke",
        "artist": "Bruce Robison & Kelly Willis Show, Paula Russell",
        "time": "6:00",
        "date": "Jul 14th 2018"
    },{
        "location": "lat:30.2696, lng:-97.7364",
        "placeName": "Cheer Up Charlies",
        "artist": "Jake Ames Beats Cancer HAAM Benefit w/ the Stacks, Why Bonnie, Caroline Says, The Sun Machine, The Infinites",
        "time": "10:15",
        "date": "Jul 14th 2018"
    },{
            "location": "lat:30.266711, lng:-97.742440",
            "placeName": "Kingdom",
            "artist": "Subset feat. Ammo",
            "time": "10:00",
            "date": "Jul 14th 2018"
    },{
        "location": "lat:30.263467, lng:-97.734693",
        "placeName": "Native Hostel",
        "artist": "Sunday Brunch with Western Youth",
        "time": "1:00",
        "date": "Jul 15th 2018"
    },{
        "location": "lat:30.296261, lng:-97.742731",
        "placeName": "Antone's Records",
        "artist": "John Fury",
        "time": "3:00",
        "date": "Jul 15th 2018"
    },{
        "location": "lat:30.267708, lng:-97.736600",
        "placeName": "Beerland",
        "artist": "Allsups, Royal Brat, Bum Out",
        "time": "8:00",
        "date": "Jul 15th 2018"
    },{
        "location": "lat:30.2696, lng:-97.7364",
        "placeName": "Cheer Up Charlies",
        "artist": "George Clanton(Brooklyn), TC Superstar, Deep Cuts",
        "time": "9:00",
        "date": "Jul 15th 2018"
    },{
        "location":  "lat:30.263413, lng:-97.727159",
        "placeName": "Hotel Vegas",
        "artist": "P.T. Banks, The Mondegreens (SEA), & Golden Lights",
        "time": "9:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.2452, lng:-97.7689",
        "placeName":  "The A.B.G.B.",
        "artist": "Them Duqaines",
        "time": "4:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.266048, lng:-97.740400",
        "placeName":  "Antones",
        "artist": "Langhorne Slim, Harvest Thieves",
        "time": "7:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.2571,  lng:-97.7237",
        "placeName":  "Stay Gold",
        "artist": "Interroband Bass Band",
        "time": "9:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.267722,  lng:-97.741123",
        "placeName":  "B.D. Rileys",
        "artist": "Irsh Tune Session",
        "time": "9:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.266884,  lng:-97.745290",
        "placeName":  "Cedar Street",
        "artist": "Rochelle & the Sidewinders",
        "time": "3:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.2505,  lng:-97.7491",
        "placeName":  "The Continental Club",
        "artist": "Heybale!",
        "time": "3:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.2795, lng:-97.6816",
        "placeName":  "The Sahara Lounge",
        "artist": "JaRon Marshall Group, Hip Modus, Side Piece, Donny Jewelz",
        "time": "8:00",
        "date": "Jul 15th 2018"
    },{ 
        "location": "lat:30.2846,  lng:-97.7049",
        "placeName":  "The Skylark Lounge",
        "artist": "Soul Man Sam",
        "time": "8:00",
        "date": "Jul 15th 2018"
    },{
        "location": "lat:30.268501, lng:-97.736258",
        "placeName": "Stubbs",
        "artist": "Gospel Brunch w/Kings of Harmony",
        "time": "10:30",
        "date": "Jul 15th 2018"
    },{
        "location": "lat:30.237973, lng:-97.739455",
        "placeName": "Whip In",
        "artist": "Brannen Temple Group",
        "time": "7:00",
        "date": "Jul 15th 2018"
    },{
        "location": "lat:30.276970, lng:-97.732165",
        "placeName": "Frank Erwin Center",
        "artist": "Smashing Pumpkins, Metric",
        "time": "8:00",
        "date": "Jul 16th 2018"
    },{
        "location": "lat:30.2696, lng:-97.7364",
        "placeName": "Cheer Up Charlies",
        "artist": "The Nothing Song Presents: Barbarian, Fringe Class (Portland), Princess Dewclaw (Denver)",
        "time": "9:00",
        "date": "Jul 16th 2018"
    },{
        "location": "lat:37.090240, lng:-95.712891",
        "placeName": "Volstead",
        "artist": "Me Mer Mo Monday!! with Sick Van, Attic Ted, Abalama, Colin McIntyre, Space Angel Hug, Energy Portal by Astral Visions and Universal Remote, DJ Bradley Booms, DJ Part Hologram",
        "time": "6:00",
        "date": "Jul 16th 2018"
    },{ 
        "location": "lat:30.2571,  lng:-97.7237",
        "placeName":  "Stay Gold",
        "artist": "Datura",
        "time": "10:00",
        "date": "Jul 16th 2018"
    },{ 
        "location": "lat:30.2505,  lng:-97.7491",
        "placeName":  "The Continental Club",
        "artist": "Alvin Crow",
        "time": "10:15",
        "date": "Jul 16th 2018"
    },{
    "location": "lat:30.2741,  lng:-97.7634",
    "placeName":  "Donn's Depot",
    "artist": "Chris Gage",
    "time": "7:00",
    "date": "Jul 16th 2018"
    },{ 
        "location": "lat:30.2682,  lng:-97.7417",
        "placeName":  "The Driskell",
        "artist": "Rob Mahoney",
        "time": "6:00",
        "date": "Jul 16th 2018"
    },{ 
        "location": "lat:30.265598,  lng:-97.743249",
        "placeName":  "The Elephant Room",
        "artist": "Michael Mordecai's Jazz Jam",
        "time": "9:30",
        "date": "Jul 16th 2018"
    },{ 
        "location": "lat:30.290062,  lng:-97.741627",
        "placeName":  "Hole In The Wall",
        "artist": "HiFiKid, Jeremiah Jackson, Willowspeak",
        "time": "9:00",
        "date": "Jul 16th 2018"
    },{ 
        "location": "lat:30.2846,  lng:-97.7049",
        "placeName":  "The Skylark Lounge",
        "artist": "The Mad Captains Choir",
        "time": "7:00",
        "date": "Jul 16th 2018"
    },{
        "location": "lat:30.253552,  lng:-97.763559",
        "placeName":  "Saxon Pub",
        "artist": "Beat Root Revival, Hoodygoode",
        "time": "6:00",
        "date": "Jul 16th 2018"
    },{
        "location": "lat:30.2668, lng:-97.7436",
        "placeName":  "Speakeasy",
        "artist": "Open mic w/ Ronnie Hall",
        "time": "9:30",
        "date": "Jul 16th 2018"
    },{
        "location": "lat:30.2626,  lng:-97.7269",
        "placeName":  "The White Horse",
        "artist": "Chuck Fleming & the Sadlands, Kathyrn Legendre and Texas Tycoons",
        "time": "8:00",
        "date": "Jul 16th 2018"
    },{
        "location": "lat:30.267708, lng:-97.736600",
        "placeName": "Beerland",
        "artist": "Vertical Vice, The Rememberables (DC), Blacksage (MD)",
        "time": "8:00",
        "date": "Jul 17th 2018"
    },{ 
    "location":  "lat:30.263413, lng:-97.727159",
    "placeName": "Hotel Vegas",
    "artist": "No Mind, Bali Yaaah, and ObE ",
    "time": "9:00",
    "date": "Jul 17th 2018"
    },{
        "location": "lat:30.2696, lng:-97.7364",
        "placeName": "Cheer Up Charlies",
        "artist": "June Pastel, James Junius, Mountebank, & More",
        "time": "8:00",
        "date": "Jul 17th 2018"
    },{
        "location": "lat:30.2408,  lng:-97.7852",
        "placeName": "Broken Spoke",
        "artist": "Weldon Henson",
        "time": "8:15",
        "date": "Jul 17th 2018"
    },{ 
        "location": "lat:30.2505,  lng:-97.7491",
        "placeName":  "The Continental Club",
        "artist": "Curtis McMurtry",
        "time": "8:30",
        "date": "Jul 17th 2018"
    },{ 
        "location": "lat:30.2682,  lng:-97.7417",
        "placeName":  "The Driskell",
        "artist": "Brian Kremer",
        "time": "8:00",
        "date": "Jul 17th 2018"
    },{ 
        "location": "lat:30.2565,  lng:-97.7020",
        "placeName":  "Hard Luck Lounge",
        "artist": "Tater Tuesdays",
        "time": "8:00",
        "date": "Jul 17th 2018"
    },{ 
        "location": "lat:30.290062,  lng:-97.741627",
        "placeName":  "Hole In The Wall",
        "artist": "Half Man",
        "time": "9:00",
        "date": "Jul 17th 2018"
    },{
        "location":  "lat:30.263413, lng:-97.727159",
        "placeName": "Hotel Vegas",
        "artist": "No Mind, Balie Yaaah, ObE",
        "time": "9:00",
        "date": "Jul 17th 2018"
    },{ 
        "location": "lat:30.2846,  lng:-97.7049",
        "placeName":  "The Skylark Lounge",
        "artist": "Dickie Lee Erwin",
        "time": "8:30",
        "date": "Jul 17th 2018"
    },{ 
        "location": "lat:30.2668,  lng:-97.7436",
        "placeName":  "Speakeasy",
        "artist": "Trent Minter",
        "time": "8:30",
        "date": "Jul 17th 2018"
    },{
    "location": "lat:30.2571,  lng:-97.7237",
    "placeName":  "Stay Gold",
    "artist": "Jazz Jam Night",
    "time": "10:00",
    "date": "Jul 17th 2018"
},{
    "location": "lat:30.2626,  lng:-97.7269",
    "placeName":  "The White Horse",
    "artist": "Johnny & the Blalocks, Devin Jake and Chansons et Soulards",
    "time": "7:00",
    "date": "Jul 17th 2018"
},{
    "location": "lat:30.309908,  lng:-97.708215",
    "placeName":  "Carousel Lounge",
    "artist": "The Ruins",
    "time": "7:00",
    "date": "Jul 17th 2018"
},{
    "location": "lat:30.266962,  lng:-97.772859",
    "placeName":  "Zilker Park",
    "artist": "Blues on the Green w/Wild Child, Los Coast",
    "time": "8:00",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.266980, lng:-97.7362",
    "placeName": "Barracuda",
    "artist": "Julianna Barwick and Mary Lattimore",
    "time": "8:00",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.2696, lng:-97.7364",
    "placeName": "Cheer Up Charlies",
    "artist": "The Nothing Song Presents: Wax Idols, SRSQ (Kennedy of Them Are Us Too)",
    "time": "9:00",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.2702, lng:-97.7360",
    "placeName": "Mohawk",
    "artist": "Super Doppler, Acid Carousel, The Millbrook Estates",
    "time": "8:00",
    "date": "Jul 18th 2018"
},{
    "location":  "lat:30.263413, lng:-97.727159",
    "placeName": "Hotel Vegas",
    "artist": "Fear and Loathing in Hotel Vegas with Nolan Potters Nightmare Band, Kay Odyssey, The Rotten Mangos",
    "time": "9:00",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.267708, lng:-97.736600",
    "placeName": "Beerland",
    "artist": "Goldbloom, Peyote Coyote, Grady Philip Drugg, Hand-Me-Down Adventure",
    "time": "8:00",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.309908,  lng:-97.708215",
    "placeName":  "Carousel Lounge",
    "artist": "Stuart Burns, Whalen Nash",
    "time": "7:00",
    "date": "Jul 18th 2018"
},{ 
    "location": "lat:30.2452, lng:-97.7689",
    "placeName":  "The A.B.G.B.",
    "artist": "Warren Hood",
    "time": "6:30",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.2571,  lng:-97.7237",
    "placeName":  "Stay Gold",
    "artist": "Ben Ballinger",
    "time": "9:30",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.2408,  lng:-97.7852",
    "placeName": "Broken Spoke",
    "artist": "Mike Stinson Blazing Bows",
    "time": "6:00",
    "date": "Jul 18th 2018"
},{
    "location": "lat:30.2741,  lng:-97.7634",
    "placeName":  "Donn's Depot",
    "artist": "Frank & the Honky-Tonk Doctors",
    "time": "7:00",
    "date": "Jul 18th 2018"
    },{ 
        "location": "lat:30.2682,  lng:-97.7417",
        "placeName":  "The Driskell",
        "artist": "Bruce Smith",
        "time": "8:00",
        "date": "Jul 18th 2018"
    },{ 
        "location": "lat:30.265598,  lng:-97.743249",
        "placeName":  "The Elephant Room",
        "artist": "Adrian Ruiz Quintet, Jitterbug Vipers",
        "time": "6:30",
        "date": "Jul 18th 2018"
    },{ 
        "location": "lat:30.2795, lng:-97.6816",
        "placeName":  "The Sahara Lounge",
        "artist": "Ferenbacher, Spirit Ghost, Sherry, She Verb",
        "time": "8:30",
        "date": "Jul 18th 2018"
    },{
        "location": "lat:30.2626,  lng:-97.7269",
        "placeName":  "The White Horse",
        "artist": "Croy and the Boys, the Motts & Rock Step Relevators",
        "time": "8:00",
        "date": "Jul 18th 2018"
    }
    

    

    

    
        
         
    ];

    db.Showlist
    .remove({})
    .then(() => db.Showlist.collection.insertMany(ShowlistSeed))
    .then(data=>{
        console.log(data.insertedCount + "records inserted!");
    })
    .catch (err =>{
    console.log.error(err);
});